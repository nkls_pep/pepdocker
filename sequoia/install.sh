#!/bin/bash

set -x

export PREFIX=${PREFIX:=/usr/local}

git clone https://gitlab.com/sequoia-pgp/sequoia.git
cd sequoia
git checkout pep-engine
more Makefile
uname -s
#sed -i '22s/.*/$(info $$INSTALL is [${INSTALL}])/' openpgp-ffi/Makefile
sed -i '23s/.*/ifneq ($(filter Darwin BSD,$(shell uname -s)),)/' openpgp-ffi/Makefile
sed -i '9s/.*/ifneq ($(filter Darwin BSD,$(shell uname -s)),)/' store/Makefile
sed -i '23s/.*/ifneq ($(filter Darwin BSD,$(shell uname -s)),)/' ffi/Makefile
sed -i '10s/.*/ifneq ($(filter Darwin BSD,$(shell uname -s)),)/' sop/Makefile
sed -i '10s/.*/ifneq ($(filter Darwin BSD,$(shell uname -s)),)/' sq/Makefile
sed -i '10s/.*/ifneq ($(filter Darwin BSD,$(shell uname -s)),)/' sqv/Makefile
sed -i '9s/.*/ifneq ($(filter Darwin BSD,$(shell uname -s)),)/' store/Makefile
sed -i '41s/.*/ifneq ($(filter Darwin BSD,$(shell uname -s)),)/' Makefile
make build-release PYTHON=disable
make install PYTHON=disable PREFIX=$PREFIX
cd ..
rm -rf sequoia $PREFIX/cargo $(whoami)/rustup $(whoami)/.cargo $(whoami)/.cache
