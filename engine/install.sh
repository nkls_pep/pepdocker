#!/bin/bash

set -x

export PREFIX=${PREFIX:=/usr/local}
mkdir -p $PREFIX

#hg clone https://pep.foundation/dev/repos/pEpEngine/
git clone https://gitea.pep.foundation/pEp.foundation/pEpEngine pEpEngine
cd pEpEngine
echo '
PREFIX='$PREFIX'
SOURCE='$SOURCE'
PER_MACHINE_DIRECTORY=$(PREFIX)/share/pEp
SYSTEM_DB=$(PER_MACHINE_DIRECTORY)/system.db

YML2_PATH=$(PREFIX)/yml2

ETPAN_LIB=-L$(PREFIX)/lib
ETPAN_INC=-I$(PREFIX)/include

ASN1C=$(PREFIX)/bin/asn1c
ASN1C_INC=-I$(PREFIX)/share/asn1c

OPENPGP=SEQUOIA
SEQUOIA_LIB=-L$(PREFIX)/lib
SEQUOIA_INC=-I$(PREFIX)/include

DEBUG=YES

export PKG_CONFIG_PATH=$(PREFIX)/share/pkgconfig/
' > local.conf
make
make install
make db dbinstall
cd .. && rm -rf pEpEngine
