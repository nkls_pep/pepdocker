#!/bin/bash

set -x

export PREFIX=${PREFIX:=/usr/local}

echo '--------Cloning yml2--------------'
git clone https://gitea.pep.foundation/fdik/yml2 yml2
#hg clone https://pep.foundation/dev/repos/yml2 yml2
# make?
cp -af yml2 $PREFIX/yml2
rm -rf yml2

echo '--------Cloning libetpan--------------'
git clone https://github.com/fdik/libetpan libetpan
cd libetpan
git pull
./autogen.sh --prefix=$PREFIX && make && make install
cd .. && rm -rf libetpan

echo '--------Cloning asn1c--------------'
git clone https://github.com/vlm/asn1c asn1c
cd asn1c
git checkout tags/v0.9.28 -b pep-engine
git checkout pep-engine
autoreconf -iv
./configure --prefix=$PREFIX && make && make install
cd .. && rm -rf asn1c
